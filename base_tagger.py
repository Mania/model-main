import argparse
import os
from PIL import Image
import logging

import numpy as np
import tensorflow as tf

if tf.__version__ != '1.4.0':
    raise ImportError('Please upgrade your tensorflow installation to v1.4.0!')

# get TF logger
log = logging.getLogger('tensorflow')
log.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# create file handler which logs even debug messages
fh = logging.FileHandler(os.path.join('logs', 'tensorflow.log'))
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
log.addHandler(fh)

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('image', metavar='img', type=str,
                    help='image relative path')

args = parser.parse_args()

MODEL_NAME = 'faster_rcnn_resnet101_kitti'

# Load model and labels
PATH_TO_CKPT = os.path.join(MODEL_NAME, 'frozen_inference_graph.pb')
PATH_TO_LABELS = os.path.join(MODEL_NAME, 'labels.pbtxt')

NUM_CLASSES = 2

detection_graph = tf.Graph()

with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

# Size, in inches, of the output images.
IMAGE_SIZE = (12, 8)


def load_image_into_numpy_array(image):
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
        (im_height, im_width, 3)).astype(np.uint8)


with detection_graph.as_default():
    with tf.Session(graph=detection_graph) as sess:
        # Input
        image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

        # Outputs
        detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
        detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
        detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = detection_graph.get_tensor_by_name('num_detections:0')

        image = Image.open(args.image)
        image_np = load_image_into_numpy_array(image)
        image_np_expanded = np.expand_dims(image_np, axis=0)

        (boxes, scores, classes, num) = sess.run(
            [detection_boxes, detection_scores, detection_classes, num_detections],
            feed_dict={image_tensor: image_np_expanded})

boxes = np.squeeze(boxes)
scores = np.squeeze(scores)
classes = np.squeeze(classes).astype(np.int32)

max_boxes = boxes.shape[0]

for i in range(min(max_boxes, boxes.shape[0])):
    box = tuple(boxes[i].tolist())
    score = scores[i]
    _class = classes[i]
    if score > 0.1:
        print(box)
        print(score)
        print(_class)
